#What is it?
Simple program which solves TSP problem. It has been created due to Epic Challenge. Our team needed to create a simple map of unknown terrain which could be observed only by an electric car (simulation of exploring Mars). It is designed as REST API because details about map were coming from arduino attached to the car. 

#Example of usage

##Request:
```
curl --request POST \
  --url http://localhost:8080/calculateShortestPath \
  --header 'content-type: application/json' \
  --cookie JSESSIONID=C58D3482DBCCDB01038E2AF4AC738EF9 \
  --data '{
	"collectableObjects": [
		{
			"coordinates":{
				"point":{
					"x": 1.1,
					"y": 11.0
				}	
			}
		},
		{
			"coordinates":{
				"point":{
					"x": 2.1,
					"y": 17.0
				}	
			}
		},
				{
			"coordinates":{
				"point":{
					"x": 9.1,
					"y": 23.0
				}	
			}
		},
		{
			"coordinates":{
				"point":{
					"x": 14.1,
					"y": 19.0
				}	
			}
		},
				{
			"coordinates":{
				"point":{
					"x": 4.1,
					"y": 51.0
				}	
			}
		},
		{
			"coordinates":{
				"point":{
					"x": 16.1,
					"y": 32.0
				}	
			}
		}
	]
}'
```
##Response:
```
{
  "shortestPath": [
    {
      "type": "CollectableObject",
      "point": {
        "x": 4.1,
        "y": 51.0
      }
    },
    {
      "type": "CollectableObject",
      "point": {
        "x": 16.1,
        "y": 32.0
      }
    },
    {
      "type": "CollectableObject",
      "point": {
        "x": 14.1,
        "y": 19.0
      }
    },
    {
      "type": "CollectableObject",
      "point": {
        "x": 9.1,
        "y": 23.0
      }
    },
    {
      "type": "CollectableObject",
      "point": {
        "x": 2.1,
        "y": 17.0
      }
    },
    {
      "type": "CollectableObject",
      "point": {
        "x": 1.1,
        "y": 11.0
      }
    }
  ],
  "totalDistance": 57.33058271723409,
  "amountOfNodes": 6,
  "amountOfObstacles": 0
}
```
#How to run it?
`./gradlew bootRun`

#What should be done
##Must have
Good refactor of code, exception handling
##Would be nice
Integration with GeoGebra - it could draw automatically shortest path
Method of calculating the shortest path could be better, or perfectly it should be configurable at the request
