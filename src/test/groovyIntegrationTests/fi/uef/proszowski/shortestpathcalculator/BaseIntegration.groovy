package fi.uef.proszowski.shortestpathcalculator

import org.springframework.boot.test.context.SpringBootTest
import org.springframework.boot.test.web.client.TestRestTemplate
import org.springframework.boot.web.server.LocalServerPort
import org.springframework.test.context.ContextConfiguration
import spock.lang.Specification

@ContextConfiguration
@SpringBootTest(
        classes = [ShortestPathCalculatorApplication],
        webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT
)
abstract class BaseIntegration extends Specification {

    @LocalServerPort @Lazy
    int port

    TestRestTemplate restTemplate = new TestRestTemplate()
    String BASE_URL = "http://localhost"
    String SHORTEST_PATH_SUFFIX = "/calculateShortestPath"
}
