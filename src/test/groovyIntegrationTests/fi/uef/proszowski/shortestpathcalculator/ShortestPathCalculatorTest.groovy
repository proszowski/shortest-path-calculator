package fi.uef.proszowski.shortestpathcalculator

import fi.uef.proszowski.shortestpathcalculator.api.ObjectOnTheMapDTO
import fi.uef.proszowski.shortestpathcalculator.api.RequestDTO
import fi.uef.proszowski.shortestpathcalculator.domain.CollectableObject
import fi.uef.proszowski.shortestpathcalculator.domain.Coordinates
import fi.uef.proszowski.shortestpathcalculator.domain.ObjectOnTheMap
import fi.uef.proszowski.shortestpathcalculator.domain.Path
import fi.uef.proszowski.shortestpathcalculator.domain.Point
import org.json.JSONArray
import org.json.JSONObject
import org.springframework.http.HttpEntity
import org.springframework.http.ResponseEntity

class ShortestPathCalculatorTest extends BaseIntegration {

    def "should return the shortest path when right coordinates are given"() {

        given:
        collectableObjects
        when:
        HttpEntity request = createRequest(collectableObjects)
        ResponseEntity response = restTemplate.postForEntity(BASE_URL + ":${port}" + SHORTEST_PATH_SUFFIX, request, String)
        JSONObject jsonResponse = new JSONObject(response.body as String)
        Path shortestPath = transformToPath(jsonResponse)
        then:
        shortestPath.evaluateDistance() == expectedShortestPath.evaluateDistance()
        where:
        collectableObjects                     || expectedShortestPath
        firstCollectionOfCollectableObjects()  || firstShortestPath()
        secondCollectionOfCollectableObjects() || secondShortestPath()
        thirdCollectionOfCollectableObjects()  || thirdShortestPath()
        fourthCollectionOfCollectableObjects() || fourthShortestPath()
    }

    private static Path transformToPath(JSONObject jsonResponse) {
        List<ObjectOnTheMap> objectsOnTheMap = []
        JSONArray objectsOnTheMapJsonArray = jsonResponse.getJSONArray("shortestPath")
        for(int i = 0; i < objectsOnTheMapJsonArray.length(); i++){
            String type = objectsOnTheMapJsonArray.getJSONObject(i).getString("type")
            Double x = objectsOnTheMapJsonArray.getJSONObject(i).getJSONObject("point").getDouble("x")
            Double y = objectsOnTheMapJsonArray.getJSONObject(i).getJSONObject("point").getDouble("y")
            Point point = new Point(x, y)
            objectsOnTheMap.add(new ObjectOnTheMapDTO(type, point).toObjectOnTheMap())
        }
        return new Path(objectsOnTheMap)
    }

    List<CollectableObject> firstCollectionOfCollectableObjects() {
        return [
                new CollectableObject(new Coordinates(new Point(100, 100))),
                new CollectableObject(new Coordinates(new Point(40, 40))),
                new CollectableObject(new Coordinates(new Point(20, 20))),
                new CollectableObject(new Coordinates(new Point(80, 80))),
                new CollectableObject(new Coordinates(new Point(60, 60))),
        ]
    }

    List<CollectableObject> secondCollectionOfCollectableObjects() {
        return [
                new CollectableObject(new Coordinates(new Point(50, 60))),
                new CollectableObject(new Coordinates(new Point(20, 20))),
                new CollectableObject(new Coordinates(new Point(60, 20))),
                new CollectableObject(new Coordinates(new Point(40, 20))),
                new CollectableObject(new Coordinates(new Point(80, 20))),
        ]
    }

    List<CollectableObject> thirdCollectionOfCollectableObjects() {
        return [
                new CollectableObject(new Coordinates(new Point(10, 20))),
                new CollectableObject(new Coordinates(new Point(40, 10))),
                new CollectableObject(new Coordinates(new Point(60, 40))),
                new CollectableObject(new Coordinates(new Point(70, 30))),
                new CollectableObject(new Coordinates(new Point(50, 50))),
        ]
    }

    List<CollectableObject> fourthCollectionOfCollectableObjects() {
        return [
                new CollectableObject(new Coordinates(new Point(100, 100))),
                new CollectableObject(new Coordinates(new Point(0, 0))),
                new CollectableObject(new Coordinates(new Point(0, 100))),
                new CollectableObject(new Coordinates(new Point(100, 0))),
        ]
    }

    Path firstShortestPath() {
                new Path(
                        [
                                new CollectableObject(new Coordinates(new Point(20, 20))),
                                new CollectableObject(new Coordinates(new Point(40, 40))),
                                new CollectableObject(new Coordinates(new Point(60, 60))),
                                new CollectableObject(new Coordinates(new Point(80, 80))),
                                new CollectableObject(new Coordinates(new Point(100, 100))),
                        ]
                )
    }

    Path secondShortestPath() {
                new Path(
                        [
                                new CollectableObject(new Coordinates(new Point(50, 60))),
                                new CollectableObject(new Coordinates(new Point(20, 20))),
                                new CollectableObject(new Coordinates(new Point(40, 20))),
                                new CollectableObject(new Coordinates(new Point(60, 20))),
                                new CollectableObject(new Coordinates(new Point(80, 20))),
                        ]
                )
    }

    Path thirdShortestPath() {
                new Path(
                        [
                                new CollectableObject(new Coordinates(new Point(10, 20))),
                                new CollectableObject(new Coordinates(new Point(40, 10))),
                                new CollectableObject(new Coordinates(new Point(70, 30))),
                                new CollectableObject(new Coordinates(new Point(60, 40))),
                                new CollectableObject(new Coordinates(new Point(50, 50))),
                        ]
                )
    }

    Path fourthShortestPath() {
                new Path(
                        [
                                new CollectableObject(new Coordinates(new Point(0, 0))),
                                new CollectableObject(new Coordinates(new Point(0, 100))),
                                new CollectableObject(new Coordinates(new Point(100, 100))),
                                new CollectableObject(new Coordinates(new Point(100, 0))),
                        ],
                )
    }

    HttpEntity createRequest(List<CollectableObject> collectableObjects) {
        RequestDTO requestDTO = new RequestDTO(collectableObjects, [], [])
        HttpEntity request = new HttpEntity(requestDTO as Object)
        return request
    }
}
