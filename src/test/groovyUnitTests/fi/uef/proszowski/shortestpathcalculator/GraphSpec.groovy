package fi.uef.proszowski.shortestpathcalculator

import fi.uef.proszowski.shortestpathcalculator.domain.CalibrationPoint
import fi.uef.proszowski.shortestpathcalculator.domain.CollectableObject
import fi.uef.proszowski.shortestpathcalculator.domain.Coordinates
import fi.uef.proszowski.shortestpathcalculator.domain.Graph
import fi.uef.proszowski.shortestpathcalculator.domain.ObjectOnTheMap
import fi.uef.proszowski.shortestpathcalculator.domain.Obstacle
import fi.uef.proszowski.shortestpathcalculator.domain.Path
import fi.uef.proszowski.shortestpathcalculator.domain.Point
import spock.lang.Specification

class GraphSpec extends Specification {

    List<ObjectOnTheMap> collectableObjects = [
            new CollectableObject(new Coordinates(new Point(0, 0))),
            new CollectableObject(new Coordinates(new Point(5, 7))),
            new CollectableObject(new Coordinates(new Point(3, 2))),
            new CollectableObject(new Coordinates(new Point(4, 8))),
            new CollectableObject(new Coordinates(new Point(10, 2))),
            new CollectableObject(new Coordinates(new Point(8, 5))),
    ]
    List<CalibrationPoint> calibrationPoints = [
            new CalibrationPoint(new Coordinates(new Point(0, 0))),
            new CalibrationPoint(new Coordinates(new Point(0, 100))),
            new CalibrationPoint(new Coordinates(new Point(100, 100))),
            new CalibrationPoint(new Coordinates(new Point(100, 0))),
    ]

    def "should generate every way of visiting every node"() {
        given:
        Integer expectedAmountOfGeneratedWays = getFactor(collectableObjects.size())
        Graph graph = new Graph(collectableObjects, [])
        when:
        graph.generateEveryPossiblePath()
        then:
        graph.possiblePaths.size() == expectedAmountOfGeneratedWays
    }

    def "should extend list of possible ways by adding to each every calibration point at the beginning and the end"() {
        given:
        Integer expectedAmountOfGeneratedWays = getFactor(collectableObjects.size())
        Graph graph = new Graph(collectableObjects, [])
        graph.generateEveryPossiblePath()
        when:
        graph.extendPossiblePathsByStartingAndReturningToEachCornerPoint(calibrationPoints)
        then:
        graph.possiblePaths.size() == expectedAmountOfGeneratedWays * 4
    }

    def "should return proper shortest paths for provided data"() {
        given:
        Graph graph = new Graph(collectableObjects, [])
        graph.generateEveryPossiblePath()
        List<Path> expectedShortestPossiblePaths = [
                new Path([
                        new CalibrationPoint(new Coordinates(new Point(0, 0))),
                        new CollectableObject(new Coordinates(new Point(0, 0))),
                        new CollectableObject(new Coordinates(new Point(3, 2))),
                        new CollectableObject(new Coordinates(new Point(10, 2))),
                        new CollectableObject(new Coordinates(new Point(8, 5))),
                        new CollectableObject(new Coordinates(new Point(5, 7))),
                        new CollectableObject(new Coordinates(new Point(4, 8))),
                        new CalibrationPoint(new Coordinates(new Point(0, 0))),
                ]
                ),
                new Path([
                        new CalibrationPoint(new Coordinates(new Point(0, 0))),
                        new CollectableObject(new Coordinates(new Point(0, 0))),
                        new CollectableObject(new Coordinates(new Point(4, 8))),
                        new CollectableObject(new Coordinates(new Point(5, 7))),
                        new CollectableObject(new Coordinates(new Point(8, 5))),
                        new CollectableObject(new Coordinates(new Point(10, 2))),
                        new CollectableObject(new Coordinates(new Point(3, 2))),
                        new CalibrationPoint(new Coordinates(new Point(0, 0))),
                ]
                ),
                new Path([
                        new CalibrationPoint(new Coordinates(new Point(0, 0))),
                        new CollectableObject(new Coordinates(new Point(3, 2))),
                        new CollectableObject(new Coordinates(new Point(10, 2))),
                        new CollectableObject(new Coordinates(new Point(8, 5))),
                        new CollectableObject(new Coordinates(new Point(5, 7))),
                        new CollectableObject(new Coordinates(new Point(4, 8))),
                        new CollectableObject(new Coordinates(new Point(0, 0))),
                        new CalibrationPoint(new Coordinates(new Point(0, 0))),
                ]
                ),
                new Path([
                        new CalibrationPoint(new Coordinates(new Point(0, 0))),
                        new CollectableObject(new Coordinates(new Point(4, 8))),
                        new CollectableObject(new Coordinates(new Point(5, 7))),
                        new CollectableObject(new Coordinates(new Point(8, 5))),
                        new CollectableObject(new Coordinates(new Point(10, 2))),
                        new CollectableObject(new Coordinates(new Point(3, 2))),
                        new CollectableObject(new Coordinates(new Point(0, 0))),
                        new CalibrationPoint(new Coordinates(new Point(0, 0))),
                ]
                ),
        ]
        when:
        graph.extendPossiblePathsByStartingAndReturningToEachCornerPoint(calibrationPoints)
        then:
        expectedShortestPossiblePaths == graph.shortestPossiblePaths
    }

    def "should include obstacles in possible paths"() {
        given:
        Graph graph = new Graph(collectableObjects, [])
        graph.generateEveryPossiblePath()
        when:
        graph.extendPossiblePathsByStartingAndReturningToEachCornerPoint(calibrationPoints)
        graph.addObstaclesToPossiblePaths(obstacles)
        then:
        println(graph.shortestPossiblePaths)
        graph.shortestPossiblePaths.contains(expectedShortestPathWithObstacles)
        where:
        obstacles               || expectedShortestPathWithObstacles
        firstListOfObstacles()  || firstExpectedShortestPathWithObstacles()
        secondListOfObstacles() || secondExpectedShortestPathWithObstacles()
    }

    private static List<Obstacle> firstListOfObstacles() {
        [
                new Obstacle(new Coordinates(new Point(5, 2))),
                new Obstacle(new Coordinates(new Point(8, 2))),
                new Obstacle(new Coordinates(new Point(10, 25))),
                new Obstacle(new Coordinates(new Point(102, 33))),
        ]
    }

    private static List<Obstacle> secondListOfObstacles() {
        [
                new Obstacle(new Coordinates(new Point(5, 2))),
                new Obstacle(new Coordinates(new Point(9, 3.5))),
        ]
    }

    private static Path firstExpectedShortestPathWithObstacles() {
        new Path([
                new CalibrationPoint(new Coordinates(new Point(0, 0))),
                new CollectableObject(new Coordinates(new Point(0, 0))),
                new CollectableObject(new Coordinates(new Point(3, 2))),
                new Obstacle(new Coordinates(new Point(5, 2))),
                new Obstacle(new Coordinates(new Point(8, 2))),
                new CollectableObject(new Coordinates(new Point(10, 2))),
                new CollectableObject(new Coordinates(new Point(8, 5))),
                new CollectableObject(new Coordinates(new Point(5, 7))),
                new CollectableObject(new Coordinates(new Point(4, 8))),
                new CalibrationPoint(new Coordinates(new Point(0, 0))),
        ])
    }

    private static Path secondExpectedShortestPathWithObstacles() {
        new Path([
                new CalibrationPoint(new Coordinates(new Point(0, 0))),
                new CollectableObject(new Coordinates(new Point(0, 0))),
                new CollectableObject(new Coordinates(new Point(3, 2))),
                new Obstacle(new Coordinates(new Point(5, 2))),
                new CollectableObject(new Coordinates(new Point(10, 2))),
                new Obstacle(new Coordinates(new Point(9, 3.5))),
                new CollectableObject(new Coordinates(new Point(8, 5))),
                new CollectableObject(new Coordinates(new Point(5, 7))),
                new CollectableObject(new Coordinates(new Point(4, 8))),
                new CalibrationPoint(new Coordinates(new Point(0, 0))),
        ])
    }

    private static Integer getFactor(Integer n) {
        Integer factor = 1
        for (i in 2..n) {
            factor *= i
        }

        return factor
    }
}