package fi.uef.proszowski.shortestpathcalculator

import fi.uef.proszowski.shortestpathcalculator.domain.CollectableObject
import fi.uef.proszowski.shortestpathcalculator.domain.Coordinates
import fi.uef.proszowski.shortestpathcalculator.domain.Obstacle
import fi.uef.proszowski.shortestpathcalculator.domain.Path
import fi.uef.proszowski.shortestpathcalculator.domain.Point
import spock.lang.Specification


class PathSpec extends Specification {

    def "should return proper distance for path consisting of nodes"() {
        given:
        Path path = new Path(
                [
                        new CollectableObject(new Coordinates(new Point(0, 0))),
                        new CollectableObject(new Coordinates(new Point(5, 7))),
                        new CollectableObject(new Coordinates(new Point(3, 2))),
                        new CollectableObject(new Coordinates(new Point(4, 8))),
                ],
        )
        Double expectedDistance = 20.070
        when:
        Double evaluatedDistance = path.evaluateDistance()
        then:
        evaluatedDistance.round(3) == expectedDistance
    }

    def "should not count obstacle when calculating distance"(){
        given:
        Path path = new Path(
                [
                        new CollectableObject(new Coordinates(new Point(0, 0))),
                        new Obstacle(new Coordinates(new Point(2, 3))),
                        new CollectableObject(new Coordinates(new Point(5, 7))),
                        new CollectableObject(new Coordinates(new Point(3, 2))),
                        new Obstacle(new Coordinates(new Point(7, 9))),
                        new CollectableObject(new Coordinates(new Point(4, 8))),
                ],
        )
        Double expectedDistance = 20.070
        when:
        Double evaluatedDistance = path.evaluateDistance()
        then:
        evaluatedDistance.round(3) == expectedDistance

    }

}