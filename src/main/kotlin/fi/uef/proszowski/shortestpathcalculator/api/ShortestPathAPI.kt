package fi.uef.proszowski.shortestpathcalculator.api

import fi.uef.proszowski.shortestpathcalculator.domain.*
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.ResponseBody
import org.springframework.web.bind.annotation.RestController

@RestController
class ShortestPathAPI {

    @ResponseBody
    @PostMapping("/calculateShortestPath")
    fun calculateShortestPath(@RequestBody requestDTO: RequestDTO): ResponseEntity<PathDTO> {
        val graph = Graph(requestDTO.collectableObjects)
        graph.generateEveryPossiblePath()
        print(requestDTO);
        graph.extendPossiblePathsByStartingAndReturningToEachCornerPoint(requestDTO.calibrationPoints)
        val shortestPossiblePath = graph.getShortestPossiblePaths().first()
        shortestPossiblePath.takeObstaclesIntoConsideration(requestDTO.obstacles)
        val pathDTO = PathDTO(shortestPossiblePath.getObjectsDTO(), shortestPossiblePath.evaluateDistance(), shortestPossiblePath.amountOfNodes(), shortestPossiblePath.amountOfObstacles())
        return ResponseEntity(pathDTO, HttpStatus.OK)
    }

    @ResponseBody
    @PostMapping("/calculateShortestPathWithoutCalibrationPoints")
    fun calculateShortestPathWithoutCalibrationPoints(@RequestBody requestDTO: RequestDTO): ResponseEntity<PathDTO>{
        val graph = Graph(requestDTO.collectableObjects)
        graph.generateEveryPossiblePath()
        val shortestPossiblePath = graph.getShortestPossiblePaths().first()
        shortestPossiblePath.takeObstaclesIntoConsideration(requestDTO.obstacles)
        val pathDTO = PathDTO(shortestPossiblePath.getObjectsDTO(), shortestPossiblePath.evaluateDistance(), shortestPossiblePath.amountOfNodes(), shortestPossiblePath.amountOfObstacles())
        return ResponseEntity(pathDTO, HttpStatus.OK)
    }
}