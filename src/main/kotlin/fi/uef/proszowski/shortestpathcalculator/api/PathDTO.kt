package fi.uef.proszowski.shortestpathcalculator.api


data class PathDTO(val shortestPath: List<ObjectOnTheMapDTO>, val totalDistance: Double, val amountOfNodes: Int, val amountOfObstacles: Int) {

}