package fi.uef.proszowski.shortestpathcalculator.api

import fi.uef.proszowski.shortestpathcalculator.domain.CalibrationPoint
import fi.uef.proszowski.shortestpathcalculator.domain.CollectableObject
import fi.uef.proszowski.shortestpathcalculator.domain.Obstacle

data class RequestDTO(val collectableObjects: List<CollectableObject> = listOf(),
                      val obstacles: List<Obstacle> = listOf(),
                      val calibrationPoints: List<CalibrationPoint> = listOf())