package fi.uef.proszowski.shortestpathcalculator.api

import fi.uef.proszowski.shortestpathcalculator.domain.CollectableObject
import fi.uef.proszowski.shortestpathcalculator.domain.Coordinates
import fi.uef.proszowski.shortestpathcalculator.domain.ObjectOnTheMap
import fi.uef.proszowski.shortestpathcalculator.domain.Obstacle
import fi.uef.proszowski.shortestpathcalculator.domain.Point

data class ObjectOnTheMapDTO(val type: String, val point: Point) {
    fun toObjectOnTheMap(): ObjectOnTheMap? {
        var toReturn: ObjectOnTheMap? = null
        when(type){
            "CollectableObject" -> toReturn = CollectableObject(Coordinates(point))
            "Obstacle" ->  toReturn = Obstacle(Coordinates(point))
        }

        return toReturn
    }
}