package fi.uef.proszowski.shortestpathcalculator

import org.springframework.boot.SpringApplication
import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication

@SpringBootApplication
class ShortestPathCalculatorApplication{
    companion object {
        @JvmStatic
        fun main(args: Array<String>) {
            SpringApplication.run(ShortestPathCalculatorApplication::class.java, *args)
        }
    }
}

