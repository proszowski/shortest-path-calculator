package fi.uef.proszowski.shortestpathcalculator.domain

class CalibrationPoint(coordinates: Coordinates) : ObjectOnTheMap(coordinates) {
    override fun toString(): String {
        return "CalibrationPoint($coordinates)"
    }
}