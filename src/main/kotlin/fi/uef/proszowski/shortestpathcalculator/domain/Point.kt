package fi.uef.proszowski.shortestpathcalculator.domain

data class Point(val x: Double, val y: Double) {
}