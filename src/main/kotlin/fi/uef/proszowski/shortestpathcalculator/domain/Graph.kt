package fi.uef.proszowski.shortestpathcalculator.domain

class Graph(private val nodes: List<ObjectOnTheMap> = listOf(),
            private val possiblePaths: MutableList<Path> = mutableListOf()) {

    fun generateEveryPossiblePath() {
        nodes.forEach {
            val path = Path()
            path.addNode(it)
            generateEveryPossiblePath(path, without(it))
        }
    }

    private fun without(node: ObjectOnTheMap): Graph {
        return Graph(
                nodes = nodes.filter { it != node } as MutableList<ObjectOnTheMap>,
                possiblePaths = possiblePaths
        )
    }

    private fun generateEveryPossiblePath(path: Path, graph: Graph) {
        if (graph.nodes.size == 1) {
            path.addNode(graph.nodes[0])
            possiblePaths.add(path)
        } else {
            graph.nodes.forEach {
                val anotherPath = path.getCopy()
                anotherPath.addNode(it)
                generateEveryPossiblePath(anotherPath, graph.without(it))
            }
        }
    }

    fun extendPossiblePathsByStartingAndReturningToEachCornerPoint(cornerPoints: List<CalibrationPoint>) {
        if( cornerPoints.isEmpty()) return
        val extendedPossiblePaths: MutableList<Path> = mutableListOf()
        possiblePaths.forEach { path ->
            cornerPoints.forEach {
                extendedPossiblePaths.add(
                        path.getCopy()
                                .addNode(it, 0)
                                .addNode(it)
                )
            }
        }
        possiblePaths.clear()
        possiblePaths.addAll(extendedPossiblePaths)
    }

    fun getShortestPossiblePaths(): List<Path>{
        val minimumDistance = possiblePaths.map{it.evaluateDistance()}.min()
        return possiblePaths.filter { it.evaluateDistance() == minimumDistance }
    }

    fun addObstaclesToPossiblePaths(obstacles: List<Obstacle>){
        possiblePaths.forEach {
            it.takeObstaclesIntoConsideration(obstacles)
        }
    }

}