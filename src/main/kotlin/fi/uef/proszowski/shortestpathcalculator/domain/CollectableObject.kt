package fi.uef.proszowski.shortestpathcalculator.domain

class CollectableObject(coordinates: Coordinates) : ObjectOnTheMap(coordinates){
    override fun toString(): String {
        return "CollectableObject($coordinates)"
    }
}
