package fi.uef.proszowski.shortestpathcalculator.domain

class Obstacle(coordinates: Coordinates) : ObjectOnTheMap(coordinates) {
    override fun toString(): String {
        return "Obstacle($coordinates)"
    }

}