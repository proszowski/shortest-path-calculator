package fi.uef.proszowski.shortestpathcalculator.domain

import java.util.*

abstract class ObjectOnTheMap(val coordinates: Coordinates){

    override fun equals(other: Any?): Boolean {
        return if(other is ObjectOnTheMap) Objects.equals(this.coordinates, other.coordinates) else false
    }

    override fun hashCode(): Int {
        return Objects.hashCode(coordinates)
    }

}
