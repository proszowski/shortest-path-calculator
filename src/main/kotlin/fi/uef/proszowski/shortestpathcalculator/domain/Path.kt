package fi.uef.proszowski.shortestpathcalculator.domain

import fi.uef.proszowski.shortestpathcalculator.api.ObjectOnTheMapDTO
import fi.uef.proszowski.shortestpathcalculator.exception.NodesActuallyExistsInThePathException
import kotlin.math.sqrt
import kotlin.math.pow

data class Path(private val nodes: MutableList<ObjectOnTheMap> = mutableListOf()) {

    fun addNode(node: ObjectOnTheMap, i: Int = nodes.lastIndex + 1): Path {
        if (nodes.contains(node) && node !is CalibrationPoint) {
            throw NodesActuallyExistsInThePathException("node $node already exists in the path $nodes.")
        }

        if (nodes.isEmpty()) {
            nodes.add(node)
        } else {
            nodes.add(i, node)
        }

        return this
    }

    fun evaluateDistance(): Double {
        var distance = 0.0
        val nodesWithoutObstacles = nodes.filter { it !is Obstacle }
        for (i in 0 until nodesWithoutObstacles.size - 1) {
            val firstPoint = nodesWithoutObstacles[i].coordinates.point
            val secondPoint = nodesWithoutObstacles[i + 1].coordinates.point
            distance += sqrt(sumOfSquaresOfABSsFromSubtractions(firstPoint, secondPoint))
        }

        return distance
    }

    fun getCopy(): Path {
        val copy = mutableListOf<ObjectOnTheMap>()
        copy.addAll(nodes)
        return Path(copy)
    }

    fun takeObstaclesIntoConsideration(obstacles: List<Obstacle>) {
        val whereToPutObstacle = mutableListOf<Pair<Int, Obstacle>>()
        obstacles.forEach {
            for (i in 0..nodes.size - 2) {
                if (isOnTheWay(nodes[i], nodes[i + 1], it)) {
                    whereToPutObstacle.add(0, Pair(i+1, it))
                }
            }
        }

        whereToPutObstacle.forEach {
            nodes.add(it.first, it.second)
        }

    }

    fun getObjectsDTO(): MutableList<ObjectOnTheMapDTO> {
        val objectsDTO = mutableListOf<ObjectOnTheMapDTO>()
        nodes.forEach {
            objectsDTO.add(ObjectOnTheMapDTO(it.javaClass.simpleName, it.coordinates.point))
        }
        return objectsDTO
    }

    fun amountOfObstacles(): Int{
        return nodes.count { it is Obstacle }
    }

    fun amountOfNodes(): Int{
        return nodes.size
    }

    private fun isOnTheWay(a: ObjectOnTheMap, b: ObjectOnTheMap, c: Obstacle): Boolean {
        if(!isBetween(a.coordinates.point, b.coordinates.point, c.coordinates.point)) return false
        val changeInY: Double = (b.coordinates.point.y - a.coordinates.point.y)
        val changeInX: Double = (b.coordinates.point.x - a.coordinates.point.x)
        val m: Double = changeInY / changeInX
        return c.coordinates.point.y == m * (c.coordinates.point.x - a.coordinates.point.x) + a.coordinates.point.y
    }

    private fun isBetween(a: Point, b: Point, c: Point): Boolean {
        return distance(a,c) + distance(c,b) == distance(a,b)
    }

    private fun distance(a: Point, b: Point): Double{
        return sqrt((a.x - b.x).pow(2) + (a.y - b.y).pow(2))

    }

    private fun sumOfSquaresOfABSsFromSubtractions(firstPoint: Point, secondPoint: Point) =
            (squareOfABSFromSubtraction(secondPoint.x, firstPoint.x)
                    + squareOfABSFromSubtraction(secondPoint.y, firstPoint.y))

    private fun squareOfABSFromSubtraction(x: Double, y: Double): Double = (x - y).pow(2)
}