package fi.uef.proszowski.shortestpathcalculator.exception

class NodesActuallyExistsInThePathException(message: String): Exception(message) {
}